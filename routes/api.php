<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::post('auth/logout', [AuthController::class,'logout']);

Route::get('vaccinations', [\App\Http\Controllers\VaccinationController::class,'index']);
Route::get('vaccination/{id}', [\App\Http\Controllers\VaccinationController::class,'findById']);
Route::post('vaccination', [\App\Http\Controllers\VaccinationController::class,'save']);
Route::put('vaccination/{id}', [\App\Http\Controllers\VaccinationController::class,'update']);
Route::delete('vaccination/{id}', [\App\Http\Controllers\VaccinationController::class,'delete']);

Route::get('locations', [\App\Http\Controllers\LocationController::class,'index']);
Route::get('location/{id}', [\App\Http\Controllers\LocationController::class,'findById']);
Route::post('location', [\App\Http\Controllers\LocationController::class,'save']);
Route::put('location/{id}', [\App\Http\Controllers\LocationController::class,'update']);
Route::delete('location/{id}', [\App\Http\Controllers\LocationController::class,'delete']);


Route::get('users', [\App\Http\Controllers\UserController::class,'index']);
Route::get('user/{id}', [\App\Http\Controllers\UserController::class,'findById']);
Route::post('user', [\App\Http\Controllers\UserController::class,'save']);
Route::put('user/{id}', [\App\Http\Controllers\UserController::class,'update']);
Route::delete('user/{id}', [\App\Http\Controllers\UserController::class,'delete']);

