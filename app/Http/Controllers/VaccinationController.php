<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use App\Models\Vaccination;

class VaccinationController extends Controller
{
    public function index() {

        $vaccinations = Vaccination::with(['users', 'location'])->get();
        return $vaccinations;
    }

    public function findById(string $id): Vaccination
    {
        $vaccination = Vaccination::where('id', $id)
            ->with(['users', 'location'])
            ->first();
        return $vaccination;
    }

    public function save(Request $request) : JsonResponse {
        $request = $this->parseRequest($request);
        /*+
        * use a transaction for saving model including relations
        * if one query fails, complete SQL statements will be rolled back
        */
        DB::beginTransaction();
        try {
            $vaccination = Vaccination::create($request->all());
            //save users

            if (isset($request['users']) && is_array($request['users'])) {
                foreach ($request['users'] as $user) {
                    $oneUser =
                        User::firstOrNew([
                            'svnr'=>$user['svnr'],
                            'first_name'=>$user['first_name'],
                            'last_name'=>$user['last_name'],
                            'phone'=>$user['phone'],
                            'email'=>$user['email'],
                            'password'=>$user['password'],
                            'gender'=>$user['gender'],
                            'birthdate'=>$user['birthdate'],
                            'is_vaccinated'=>$user['is_vaccinated'],
                            'is_admin'=>$user['is_admin'],
                        ]);
                    $vaccination->users()->save($oneUser);
                }
            }

            if (isset($request['locations']) && is_array($request['locations'])) {
                foreach ($request['locations'] as $loc) {
                    $location =
                        Location::firstOrNew([
                            'description'=>$loc['description'],
                            'street'=>$loc['street'],
                            'postalcode'=>$loc['postalcode'],
                            'city'=>$loc['city'],
                        ]);
                    $vaccination->locations()->save($location);
                }
            }

            DB::commit();
            // return a vaild http response
            return response()->json($vaccination, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving vaccination failed: " . $e->getMessage(), 420);
        }
    }



    public function update(Request $request, string $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $vaccinations = Vaccination::with(['users'])
                ->where('id', $id)->first();

            if ($vaccinations != null) {
                $request = $this->parseRequest($request);
                $vaccinations->update($request->all());

                $vaccinations->save();
            }
            DB::commit();

            $vaccinations1 = Vaccination::with(['users', 'location'])
                ->where('id', $id)->first();
            // return a valid http response
            return response()->json($vaccinations1, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("updating vaccination failed: " . $e->getMessage(), 420);
        }
    }


    public function delete(string $id) : JsonResponse
    {
        $vaccination = Vaccination::where('id', $id)->first();
        if ($vaccination != null) {
            $vaccination->delete();
            //Test
        }
        else
            throw new \Exception("Vaccination couldn't be deleted - it does not exist");
        return response()->json('Vaccination (' . $id . ') successfully deleted', 200);
    }


    /**
     * modify / convert values if needed
     */
    private function parseRequest(Request $request) : Request {
        // get date and convert it - its in ISO 8601, e.g. "2018-01-01T23:00:00.000Z"
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
