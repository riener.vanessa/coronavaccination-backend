<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use App\Models\Vaccination;


class LocationController extends Controller
{
    public function index() {

        $locations = Location::with(['vaccinations'])->get();
        return $locations;
    }

    public function findById(string $id): Location
    {
        $vaccination = Location::where('id', $id)
            ->with(['vaccinations'])
            ->first();
        return $vaccination;
    }

    public function save(Request $request): JsonResponse
    {
        /*+
        * use a transaction for saving model including relations
        * if one query fails, complete SQL statements will be rolled back
        */
        DB::beginTransaction();
        try {
            $location = Location::create($request->all());
            // save images
            if (isset($request['vaccinations']) && is_array($request['vaccinations'])) {
                foreach ($request['vaccinations'] as $vacc) {
                    $vaccination =
                        Vaccination::firstOrNew(['date' => $vacc['date'], 'time' => $vacc['time'],
                            'max_persons' => $vacc['max_persons']]);
                    $location->vaccinations()->save($vaccination);
                }
            }
            DB::commit();
            // return a vaild http response
            return response()->json($location, 201);
        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving vaccination failed: " . $e->getMessage(), 420);
        }
    }

    public function update(Request $request, string $id): JsonResponse
    {
        DB::beginTransaction();
        try {
            $location = Location::with(['vaccinations'])
                ->where('id', $id)->first();
            if ($location != null) {
                $location->update($request->all());
            }
            DB::commit();
            $loc1 = Location::with(['vaccinations'])
                ->where('id', $id)->first();
            return response()->json($loc1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating location failed: " . $e->getMessage(), 420);
        }
    }


    public function delete(string $id) : JsonResponse
    {
        $location = Location::where('id', $id)->first();
        if ($location != null) {
            $location->delete();
        }
        else
            throw new \Exception("location couldn't be deleted - it does not exist");
        return response()->json('location (' . $id . ') successfully deleted', 200);
    }


}
