<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index() {

        $users = User::all();
        return $users;

    }

    public function findById(string $id): User
    {
        $vaccination = User::where('id', $id)
            ->first();
        return $vaccination;
    }

    public function save(Request $request): JsonResponse
    {
        /*+
        * use a transaction for saving model including relations
        * if one query fails, complete SQL statements will be rolled back
        */
        DB::beginTransaction();
        try {
            $user = User::create($request->all());

            DB::commit();
            // return a vaild http response
            return response()->json($user, 201);
        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving user failed: " . $e->getMessage(), 420);
        }
    }


    public function update(Request $request, string $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $users = User::where('id', $id)->first();

            if ($users != null) {
                $users->update($request->all());

                $users->save();
            }
            DB::commit();

            $users1 = User::where('id', $id)->first();
            // return a valid http response
            return response()->json($users1, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("updating user failed: " . $e->getMessage(), 420);
        }
    }


    public function delete(string $id) : JsonResponse
    {
        $user = User::where('id', $id)->first();
        if ($user != null) {
            $user->delete();
        }
        else
            throw new \Exception("User couldn't be deleted - it does not exist");
        return response()->json('User (' . $id . ') successfully deleted', 200);
    }
}
