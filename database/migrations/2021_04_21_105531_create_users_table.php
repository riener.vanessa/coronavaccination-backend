<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            //Wichtige Daten, deshalb nicht nullable
            $table->bigInteger('svnr')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->string('password');
            //Geschlecht nicht relevant + Geburtstag kann aus
            //SVNR ausgelesen werden, deshalb nullable
            $table->string('gender')->nullable();
            $table->date('birthdate')->nullable();
            //Default 0 = NOPE
            $table->boolean('is_vaccinated')->default('0');
            $table->boolean('is_admin')->default('0');
            $table->foreignId('vaccination_id')->nullable()->constrained()->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

    }
}
