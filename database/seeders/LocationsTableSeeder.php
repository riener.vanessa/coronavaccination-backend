<?php

namespace Database\Seeders;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Book;
use App\Models\Vaccination;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $location = new \App\Models\Location;
        $location->description = "Design Center";
        $location->street = "Design-Center-Straße 1";
        $location->postalcode = 4020;
        $location->city = "Linz";
        $location->save();

        $location2 = new \App\Models\Location;
        $location2->description = "Hausarzt Dr. Wagner";
        $location2->street = "Linzerstraße 5";
        $location2->postalcode = 4020;
        $location2->city = "Linz";
        $location2->save();

        $location3 = new \App\Models\Location;
        $location3->description = "Hausarzt Dr. Hofer";
        $location3->street = "Breitwiesergutstraße 35";
        $location3->postalcode = 4190;
        $location3->city = "Bad Leonfelden";
        $location3->save();

    }
}
