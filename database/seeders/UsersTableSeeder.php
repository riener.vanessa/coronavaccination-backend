<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User;
        $user->svnr = '3425141098';
        $user->first_name = 'Vanessa';
        $user->last_name = 'Riener';
        $user->phone = '0699123456789';
        $user->email = 'test@mail.at';
        $user->password = bcrypt('secret');
        $user->gender = 'weiblich';
        $user->birthdate = '1998-10-14';
        $user->is_vaccinated = '0';
        $user->is_admin = '1';

        $vaccination = \App\Models\Vaccination::all()->first();
        $user->vaccination()->associate($vaccination);
        $user->save();

        $user2 = new \App\Models\User;
        $user2->svnr = '4567890234';
        $user2->first_name = 'Sarah';
        $user2->last_name = 'Riener';
        $user2->phone = '0699123456789';
        $user2->email = 'sarah@mail.at';
        $user2->password = bcrypt('secret');
        $user2->gender = 'weiblich';
        $user2->birthdate = '1995-07-06';
        $user2->is_vaccinated = '0';
        $user2->is_admin = '0';
        $user2->save();


        $user3 = new \App\Models\User;
        $user3->svnr = '6789543267';
        $user3->first_name = 'Fabian';
        $user3->last_name = 'Möstl';
        $user3->phone = '0699123456789';
        $user3->email = 'fabian@mail.at';
        $user3->password = bcrypt('secret');
        $user3->gender = 'männlich';
        $user3->birthdate = '1998-10-14';
        $user3->is_vaccinated = '0';
        $user3->is_admin = '0';
        $user3->save();



        $user4 = new \App\Models\User;
        $user4->svnr = '5678901234';
        $user4->first_name = 'Constanze';
        $user4->last_name = 'Riener';
        $user4->phone = '0699123456789';
        $user4->email = 'conny@mail.at';
        $user4->password = bcrypt('secret');
        $user4->gender = 'weiblich';
        $user4->birthdate = '1964-05-10';
        $user4->is_vaccinated = '0';
        $user4->is_admin = '0';
        $user4->vaccination()->associate($vaccination);
        $user4->save();

    }
}
