<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;


class VaccinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vaccination = new \App\Models\Vaccination;
        $vaccination->date = "2021-05-20";
        $vaccination->time= "15:00:00";
        $vaccination->max_persons = "2";

        $location = \App\Models\Location::all()->first();
        $vaccination->location()->associate($location);
        $vaccination->save();

    }
}
